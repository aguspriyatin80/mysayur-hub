const { Router } = require('express');
const router = Router();
const userRoutes = require('./user');
const UserController = require('../controllers/user');


router.get('/', (req,res)=>{
    res.status(200).json({
        msg: "this is home page"})
});

router.use('/users', userRoutes)
router.post('/login', UserController.login)
router.post('/register', UserController.register)

module.exports = router;