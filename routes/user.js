const { Router } = require('express');
const router = Router();
const UserController = require('../controllers/user')
const { authentication, authorization} = require('../middlewares/auth')
const { uploadUser } = require('../middlewares/multer')

router.get('/', authentication, UserController.list)
router.get('/detail/:id', authentication, authorization, UserController.findById)
// router.get('/delete/:id', authentication, isUser, UserController.deleteUser)

// router.get('/edit/:id', authentication, isUser, UserController.editFormUser)
// router.get('/add', authentication, isUser,UserController.addFormUser)
router.put('/edit/:id', uploadUser.single('profile_image'), UserController.editUser)

module.exports = router;

