const jwt = require('jsonwebtoken')
const secretKey = process.env.SECRET_KEY


const tokenGenerator = (user) => {
    const { id, email} = user

    return jwt.sign({
        id,
        email
    }, secretKey)
}
const tokenGeneratorAdmin = (admin) => {
    const { id, email} = admin

    return jwt.sign({
        id,
        email
    }, secretKey)
}
const tokenVerifier = (access_token) => {
    return jwt.verify(access_token, secretKey)
}

module.exports = {
    tokenGenerator, tokenVerifier, tokenGeneratorAdmin
}