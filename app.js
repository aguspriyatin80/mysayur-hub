const express = require('express');
const app = express();
var multer  = require('multer')
var uploads = multer({ dest: './uploads/users' })
// const path = require('path')
const sequelize = require('sequelize');
const cors = require('cors')
require('dotenv').config();

const PORT = process.env.PORT || 4000;
const router = require('./routes');

//Middlewares
app.use(cors())

app.use('/uploads/users', express.static('uploads/users'));
// app.use('/uploads/users', express.static(path.join(__dirname,'uploads/users')))
app.use(express.json());
app.use(express.urlencoded({ extended: true}));

//Routes
app.use(router);

app.listen(PORT, () => {
    console.log(`Server is running at port : ${PORT}`);
});
