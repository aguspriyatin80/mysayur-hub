'use strict';
const {encryptPwd} = require('../helpers/bcrypt')
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  User.init({
    full_name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    profile_image: DataTypes.STRING,
    description: DataTypes.STRING,
    notification: DataTypes.STRING,
    role: DataTypes.STRING
  }, 
  {
    hooks : {
      beforeCreate(user){
        user.password = encryptPwd(user.password)
        user.roles = 'Buyer'
      }
    },      
    sequelize,
    modelName: 'User',
  });
  return User;
};