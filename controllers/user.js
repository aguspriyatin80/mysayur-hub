const { User } = require("../models");
const {decryptPwd} = require('../helpers/bcrypt')
const {tokenGenerator} = require('../helpers/jwt')

class UserController {
	static async register(req, res) {
		const { full_name, email, password, role } = req.body;
		try {
			const found = await User.findOne({
				where: {
					email
				}
			})
			if (found) {
				res.status(409).json({
					msg: "Email already registered! Input another email account, thanks!"
				})
			} else {
				const userReg = await User.create({
					full_name,
					email,
					password,
					role
				});
				const access_token = tokenGenerator(userReg);
				res.status(201).json({access_token});
			}
		} catch (err) {
			res.status(500).json(err);
		}
	}
	  static async login(req, res, next) {
		const { email, password } = req.body;
		console.log(req.body);
		try {
			const user = await User.findOne({
				where: { email }
			});
			if (user) {
				if(decryptPwd(password, user.password)) {
				const access_token = tokenGenerator(user);
				res.status(200).json({ access_token });
			  } else {
				  res.status(409).json({
				  msg: "Incorrect password!"
				})
			  }
			} else{
                res.status(404).json({
                    msg : "User not found!"
                })
            }
			} catch (err) {
			  next(err);
			}
	}
	
	static async findById(req, res) {
		const id = req.params.id;
		try {
			const userId = await User.findOne({
				where: { id }
			});
			if (userId) {
                res.status(200).json(userId)    
            }
            else {
                res.status(404).json(`User is not found.`)
            }
        } 
        catch (error) {
            next(error)
        }
	}
	static async list(req, res, next) {
        try {
            const userList = await User.findAll({
                order : [
                    ['id','DESC']
                ]
            })
            res.status(200).json(userList)
        } catch (err) {
            res.status(500).json({
                message: error
            })
        }
    }

    static async editUser(req, res, next) { 
		const id = req.params.id;
		const { full_name, email, description } = req.body;
		const profile_image = req.file.path;
		// const profile_image = req.profile_image.path;
		try {
			const found = await User.update({
                full_name, email, profile_image, description 
            }, {
                where: { id }
            })   
            if (found) {
				User.update({
                    full_name,					
					email,
					profile_image,
					description
				}, {
					where: { id },
					}
				);
			}   
			res.status(202).json({
				msg : "Successfully updated your profile!"
		}); 
		} catch (err) {
			res.status(500).json(err);
		}
	}
	
	
}
module.exports = UserController;