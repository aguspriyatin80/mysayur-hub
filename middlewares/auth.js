const {  Review } = require('../models')
const { tokenVerifier } = require('../helpers/jwt')

class Auth {
    static async authentication(req, res, next) {
        console.log("Authentication works!")
        const { access_token } = req.headers;

        if (!access_token) {
            res.status(404).json("Token not found!")
        } else {
            try {
                const decode = tokenVerifier(access_token);
                req.userData = decode
                next();
            } catch (err) {
                res.status(500).json(err)
            }
        }
    }


    static async authorization(req, res, next) {
        console.log("Authorization works!");
        const id = req.params.id;
        const UserId = req.userData.id
        try {
            const found = await User.findOne({
                where: {
                    id
                }
            });
            if (found) {
                if (found.id === UserId) {
                    next();
                } else {
                    res.status(403).json("User doesn't have access!");
                }
            } else {
                res.status(404).json("User not found!")
            }
        } catch (err) {
            res.status(500).json(err)
        }
    }
}




module.exports = Auth;
