const multer = require("multer");

const filterImage = (req, file, cb) => {
  if (file.mimetype === 'image/jpg' || file.mimetype === 'image/png') {
      cb(null, true);
  } else {
      cb(null, 'must be upload image format jpg or png');
  }
};





const storageUser = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './uploads/users');
  },
  filename: (req, file, cb) => {
    cb(null, `${Date.now()}--${file.originalname}`);
  },
});
const uploadUser = multer({storage: storageUser,fileFilter: filterImage});


module.exports = {uploadUser};
